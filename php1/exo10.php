<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 10</h3>
    <p>A partir d’un montant à payer et d’une somme versée pour régler un achat, écrire l’algorithme qui
affiche à un utilisateur un rendu de monnaie en nombre de billets de 10 € et 5 €, de pièces de 2 € et
1 €
</p>
    <?php
        function rendu($pay,$prix)
       { 
        
        $reste = $pay-$prix;
        $r = $reste; // to keep the rest before changing it
        $nbr10 = intdiv($reste,10);
        //intdiv() return the integer part of the division
        $reste = $reste-$nbr10*10;
        $nbr5 = intdiv($reste,5);
        $reste = $reste-$nbr5*5;
        $nbr2 = intdiv($reste,2);
        $nbr1 = $reste - $nbr2*2;
        echo " montat à payer $prix <br> ";
        echo " montat à versé $pay <br> ";
        echo " montat à rendre $r <br> ";
        echo " rendu de monnaie <br> ";
        echo "$nbr10 billet de 10 € , $nbr5 billet de 5 € $nbr2 pièce de 2 € et $nbr1 pièce de 1 €";
    }

    rendu(400,313);
    ?>



</body>
</html>