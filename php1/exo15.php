<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 14</h3>
    <p>Créer une classe Personne (nom, prénom et date de naissance).
Instancier 2 personnes et afficher leurs informations : nom, prénom et âge.
$p1 = new Personne("DUPONT", "Michel", "1980-02-19") ;
$p2 = new Personne("DUCHEMIN", "Alice", "1985-01-17") ;
</p>
    <?php
       class Personne {
        private string $_nom;
        private string $_prenom;
        private DateTime $_age;


        public function __construct(string $nom,string $prenom, string $age) {

            $this->_nom = $nom;
            $this->_prenom = $prenom;
            $this->_age = New DateTime ($age);
        
        }

        public function getNom(){
            return $this->_nom;
        }
        public function getPrenom(){

           return $this->_prenom;
        }

        public function getAge(){

            return $this->_age;
        }

        

         public function setNom($nom){
            $this->_nom = $nom;
        }
        public function setPrenom($prenom){

            $this->_prenom = $prenom;
        }

        public function setAge($age){

            $this->_age = $age;
        }
        

        public function calculAge(){
            $now = date('Y-m-d');
            $date = new DateTime($this->getAge());
            $interval = $now->diff($date);
                return $interval->format('%Y');


        }


        public function affiche(){
            $now = New DateTime();
            $date =$this->getAge();
            $interval = $now->diff($date);
            $interval = $interval->y;
            echo"". $this->getNom() ." ". $this->getPrenom() ." a " . $interval ." ans";
            
        }

       }

       $p1 = new Personne("KOUZEHA","Ammar","1988-12-6");
       $p2 = new Personne("SCHAEFFER","Sandrine","1993-10-8");

       $p2->affiche();
       echo "<br>";
       $p1->affiche();
       

    ?>



</body>
</html>