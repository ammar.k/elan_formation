<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 7</h3>
    <p>Ecrire un algorithme permettant de renvoyer la catégorie d’un enfant en fonction de son âge :
</p>
    <?php
        function category($age){

            $tab = ["Poussin","Pupile","Minime","Cadet"];
            if($age>=6&&$age<=7){
                echo "L’enfant qui a $age ans appartient à la catégorie « $tab[0] »";
            }
            else if($age>=8&&$age<=9){
                echo "L’enfant qui a $age ans appartient à la catégorie « $tab[1] »";
            }
            else if($age>=10&&$age<=11){
                echo "L’enfant qui a $age ans appartient à la catégorie « $tab[2] »";
            }
            else if($age>=12){
                echo "L’enfant qui a $age ans appartient à la catégorie « $tab[3] »";
            }

            else {
                echo "désolé, cette catégorie n'est pas gerée";
            }
    
        }
        category(15);

        //another possibilty with the switch function.
    ?>



</body>
</html>