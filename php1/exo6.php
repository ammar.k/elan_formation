<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 6</h3>
    <p>Ecrire un algorithme qui déclare une valeur en francs et qui la convertit en euros.
    Attention, la valeur générée devra être arrondie à 2 décimales</p>
    <?php
        function facture($nbrArticle,$prixUnit){

            
            $fac = $nbrArticle*$prixUnit;
            $prix = $fac*0.2+$fac;
            echo "Prix unitaire de l’article : $prixUnit";
            echo"<br>";
            echo "Quantité : $nbrArticle" ;
            echo"<br>";
            echo "Taux de TVA : 0.2";
            echo"<br>";
            echo "Le montant de la facture à régler est de : $prix";
    
        }
        facture(9.99,5);
    ?>



</body>
</html>