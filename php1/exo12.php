<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 12</h3>
    <p>A partir d’une fonction personnalisée et à partir d’un tableau de prénoms et de langue associée
(tableau contenant clé et valeur), dire bonjour aux différentes personnes dans leur langue
respective (français ➔ « Salut », anglais ➔ « Hello », espagnol ➔ « Hola »)
Exemple : tableau ➔ Mickaël -> FRA, Virgile -> ESP, Marie-Claire -> ENG

</p>
    <?php
    function affiche($tab){
        foreach($tab as $key => $value) {
        
            switch($value){
                case 'FRA' : echo "Salut " . $key ."<br>"; break;
                case 'ESP' : echo "Hola " . $key ."<br>"; break;
                case 'ENG' : echo "Hello " . $key ."<br>"; break;
               default : echo "pas traité";
        }

    }

    
}
$prenoms = [
    "Mickaël" => "FRA",
    "virgile" => "ESP",
    "Marie-Claire" => "ENG"
];
affiche($prenoms);
    ?>



</body>
</html>