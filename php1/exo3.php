<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Exercice 3</h3>
    <p>A partir de la phrase de l’exercice 1, écrire l’instruction permettant de remplacer le mot
    « aujourd’hui » par le mot « demain ». Afficher l’ancienne et la nouvelle phrase.</p>
    <?php
        $tab = ["Notre", "formation DL", "commence", "aujourd'hui"];

        echo "avant la modification :";
        echo "<br>";
        echo implode(" ", $tab) ;
        echo "<br>";
        $tab[3] = "demain";
        echo "après la modification :";
        echo "<br>";
        echo implode(" ", $tab) ;
        echo "<br>";
        echo"=========================================================";
        //str replace
    ?>



</body>
</html>