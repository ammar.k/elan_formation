<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>11- Exercice 11</h1>
    <p>Ecrire une fonction personnalisée qui affiche une date en français
        (en toutes lettres) à partir d’une chaîne de caractère représentant une date.</p>
</body>
</html>

<?php
    function afficheDate($date){
        setlocale(LC_ALL, 'fr_FR.UTF8', 'fr_FR','fr','fr','fra','fr_FR@euro');
        $result= "<p style='color:red;' >La date est : </p>";
        $result.= strftime('%A %d-%m-%Y',strtotime($date));
        return $result;
    }

    echo afficheDate("2011-03-18");

    ?>