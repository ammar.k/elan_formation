<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>7- Exercise 7</h1>
    
            <p>Créer une fonction personnalisée permettant de générer des cases à cocher.
                On pourra préciser dans le tableau associatif si la case est cochée ou non.
Exemple :
genererCheckbox($elements);
//où $elements est un tableau associatif clé => valeur avec 3 entrées.</p>
</body>
</html>

<?php
    function afficheCheckbox($elements){
        
        $result= "<label for= '$elements'> choose a value :</label><br>";
        $result.= "<form action='exo7.php' method='post'>"; // action='exo7.php' pour envoyer les données vers exo7.php
        foreach($elements as $key => $value){      // $key pour récupérer les données dans exo7.php
            $result.= "<input type='checkbox' name='$key' value='$value'>$value<br>";
            
        }
        $result.= "<input type='submit' value='Envoyer'>";// value='Envoyer' pour afficher le bouton envoyer
        $result.= "</form>";
        return $result;
    }

    $elements = array("choix1" => "First", "choix2" => "Second", "choix3" => "Third");
    echo afficheCheckbox($elements);
?>