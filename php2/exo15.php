<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>15- Exercice 15</h1>
    <p>En utilisant les ressources de la page http://php.net/manual/fr/book.filter.php,
        vérifier si une adresse e-mail a le bon format.</p>
</body>
</html>

<?php
    function verifMail($mail){
        if(filter_var($mail,FILTER_VALIDATE_EMAIL)){
            $results= "L'adresse mail <p style='color:green;'>$mail</p> est valide <br>";

        }else{
            $results= "L'adresse mail <p style='color:red;'>$mail</p> n'est pas valide <br>";;
        }
        return $results;
    }

    echo verifMail("ammarkouzeha@gmail.com");
    echo verifMail("ammarkouzehagmailcom");

?>