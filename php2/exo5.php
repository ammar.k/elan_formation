<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>5- Exercice 5</h1>

        <p>Créer une fonction personnalisée permettant de créer un formulaire
             de champs de texte en précisant le nom des champs associés.
Exemple :
$nomsInput = array("Nom","Prénom","Ville"); afficherInput($nomsInput);</p>
</body>
</html>

<?php
        
        function afficherInput($nomsInput){
    
        $result= "<form action='exo5.php' method='post'>"; // action='exo5.php' pour envoyer les données vers exo5.php
        foreach($nomsInput as $value){
            $result.= "<input type='text' name='$value' placeholder='$value'><br>";
            // name='$value' pour récupérer les données dans exo5.php
        }
        $result.= "<input type='submit' value='Envoyer'>";
        $result.= "</form>";

        return $result;
    }

    $nomsInput = array("Nom","Prénom","Ville");
    echo afficherInput($nomsInput);

?>
