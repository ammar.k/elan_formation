<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>10- Exercice 10</h1>
    <p>n utilisant l’ensemble des fonctions personnalisées crées précédemment,
        créer un formulaire complet qui contient les informations suivantes :
            champs de texte avec nom, prénom, adresse e- mail, ville,
            sexe et une liste de choix parmi lesquels on pourra sélectionner un intitulé de formation :
                « Développeur Logiciel », « Designer web », « Intégrateur » ou « Chef de projet ».
        Le formulaire devra également comporter un bouton permettant
        de le soumettre à un traitement de validation (submit).
</p>
</body>
</html>

<?php

/* VEUILLEZ noter que j'ai essayé de faire une fonction pour afficher la liste déroulante mais ça n'a pas marché
le problème est que l'élements de la liste déroulante ne s'affiche au début du formulaire
quand ils doivent s'afficher en bas du formulaire
*/

/* function afficheListe($elements){
    //$results= "<label for='$elements'>Liste déroulante</label><br>";
    $results.= "<select id='$elements'>";
    $results.= "<option value=''>--Please choose an option--</option>";
    foreach($elements as $value){
        $results.= "<option value='$value'>$value</option>";
    }
    $results.= "</select>";
    $results.= "<br>";
    return $results;
} */

    function afficherFormulaire($nomsInput){
        $results = "<form action='exo10.php' method='post'>";//action='exo10.php' pour envoyer les données vers exo10.php
        foreach($nomsInput as $value){
            while($value != "Sexe" && $value != "Formation"){
                $results .= "<input type='text' name='$value' placeholder='$value'><br>";
                break;
            }
            // name='$value' pour récupérer les données dans exo10.php
            if($value == "Sexe"){
                $elements = array("Homme","Femme");
                //echo afficheListe($elements);
                $results.= "<select id='$elements'>";
                $results.= "<option value=''>--Please choose an option--</option>";
                foreach($elements as $value){
                $results.= "<option value='$value'>$value</option>";
                }
                $results.= "</select>";
                $results.= "<br>";
            }
            elseif($value == "Formation"){
                $elements = array("Développeur Logiciel","Designeur Web","Intégrateur","Chef de projet");
                //echo afficheListe($elements);
                $results.= "<select id='$elements'>";
                $results.= "<option value=''>--Please choose an option--</option>";
                foreach($elements as $value){
                $results.= "<option value='$value'>$value</option>";
                }
                $results.= "</select>";
                $results.= "<br>";
            }

        }
        $results.= "<input type='submit' value='Envoyer'>";
        $results.= "</form>";
        return $results;
    }


    $nomsInput = array("Nom","Prénom","Adresse mail","Ville","Sexe","Formation");
    echo afficherFormulaire($nomsInput);
?>