<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>2- Exercice 2</h1>

        <p>Soit le tableau suivant :
$capitales = array ("France"=>"Paris","Allemagne"=>"Berlin","USA"=>"Washington","Italie"=>"Rome");
Réaliser un algorithme permettant d’afficher le tableau HTML suivant (notez que le nom du pays
 s’affichera en majuscule et que le tableau est trié par ordre alphabétique du nom de pays) grâce à une fonction personnalisée.
Vous devrez appeler la fonction comme suit : afficherTableHTML($capitales);</p>



</body>
</html>

<?php

$capitales = array ("France"=>"Paris","Allemagne"=>"Berlin","USA"=>"Washington","Italie"=>"Rome");

function afficherTableHTML($capitales){
    echo "<table border='1' style='border-collapse: collapse; width: 50%; text-align: center;'>";
    echo "<tr>";
    echo "<th>Pays</th>";
    echo "<th>Capitale</th>";
    echo "</tr>";
    asort($capitales); // tri par ordre alphabétique
    foreach($capitales as $key => $value){
        echo "<tr>"; // ligne
        echo "<td>" . strtoupper($key) . "</td>"; // strtoupper() pour mettre en majuscule
        echo "<td>" . $value . "</td>"; // colonne
        echo "</tr>";
    }
    echo "</table>";


}

     afficherTableHTML($capitales);

?>