<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1- Exercice 1</h1>

        <p>Créer une fonction personnalisée convertirMajRouge() permettant de
            transformer une chaîne de caractère passée en argument en majuscules et en rouge.
Vous devrez appeler la fonction comme suit : convertirMajRouge($texte) ;</p>
</body>
</html>

<?php

function convertirMajRouge($texte){
    return "<p style='color:red; text-transform:uppercase;'>$texte</p>";
}

echo convertirMajRouge("Bonjour tout le monde");

?>
