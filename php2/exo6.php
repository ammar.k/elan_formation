<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>6- Exercise 6</h1>
    
            <p>Créer une fonction personnalisée permettant de remplir une liste déroulante
                à partir d'un tableau de valeurs.
                Exemple :
                        $elements = array("Monsieur","Madame","Mademoiselle"); alimenterListeDeroulante($elements);</p>
</body>
</html>

<?php
    function afficheListe($elements){
        $result= "<label for='$elements'>Liste déroulante</label><br>";
        $result.= "<select id='$elements'>";
        $result.= "<option value=''>--Please choose an option--</option>";
        foreach($elements as $value){
            $result.= "<option value='$value'>$value</option>";
        }
        $result.= "</select>";
        return $result;
    }

    $elements = array("Monsieur","Madame","Mademoiselle");
    echo afficheListe($elements);
?>