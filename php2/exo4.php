<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>4- Exercice 4</h1>

    <p>A partir de l’exercice 2, ajouter une colonne supplémentaire dans le tableau HTML qui contiendra 
        le lien hypertexte de la page Wikipédia de la capitale (le lien hypertexte devra s’ouvrir dans un nouvel onglet et le tableau sera trié par ordre alphabétique de la capitale).
On admet que l’URL de la page Wikipédia de la capitale adopte la forme :
https://fr.wikipedia.org/wiki/
Le tableau passé en argument sera le suivant :
$capitales = array ("France"=>"Paris","Allemagne"=>"Berlin", "USA"=>"Washington","Italie"=>"Rome","Espagne"=>"Madrid");</p>
</body>
</html>

<?php

$capitales = array ("France"=>"Paris","Allemagne"=>"Berlin", "USA"=>"Washington","Italie"=>"Rome","Espagne"=>"Madrid");
function afficherTableHTML($capitales){
    $result = "<table border='1 solid black;' style='border-collapse: collapse; width: 50%; text-align: center;color: green;'>";
    $result.= "<thead>";
    $result.= "<tr>";
    $result.= "<th>Pays</th>";
    $result.= "<th>Capitale</th>";
    $result.= "<th>Lien Wiki</th>";
    $result.= "</tr>";
    $result.= "</thead>";
    asort($capitales); // tri par ordre alphabétique
    $result.= "<tbody>";
    foreach($capitales as $pays => $capitale){
        $result.= "<tr>"; // ligne
        $result.= "<td>" . strtoupper($pays) . "</td>"; // strtoupper() pour mettre en majuscule
        $result.= "<td>" . $capitale . "</td>"; // colonne
        $result.= "<td><a href='https://fr.wikipedia.org/wiki/$capitale' target='_blank'>Lien</a></td>";
        $result.= "</tr>";
    }
    $result.= "</tbody>";
    $result.= "</table>";

    return $result;
}

echo afficherTableHTML($capitales);






?>