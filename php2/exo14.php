<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>14- Exercice 14</h1>
    <p>Créer une classe Voiture possédant 2 propriétés (marque et modèle) ainsi qu’une
        classe VoitureElec qui hérite (extends) de la classe Voiture et qui a une propriété supplémentaire (autonomie).
Instancier une voiture « classique » et une voiture « électrique » ayant les caractéristiques suivantes :
Peugeot 408 : $v1 = new Voiture("Peugeot","408"); BMW i3 150 : $ve1 = new VoitureElec("BMW","I3",100);
Votre programme de test devra afficher les informations des 2 voitures de la façon suivante :
echo $v1->getInfos()."<br/>";
echo $ve1->getInfos()."<br/>";</p>
</body>
</html>


<?php
    class Voiture {
        private string $_marque;
        private string $_modele;

        public function __construct(string $marque, string $modele){
            $this->_marque = $marque;
            $this->_modele = $modele;
        }
        public function getMarque(){
            return $this->_marque;
        }

        public function setMarque($marque){
            $this->_marque = $marque;
        }
        
        public function getModele(){
            return $this->_modele;
        }

        public function setModele($modele){
            $this->_modele = $modele;
        }

        public function getInfos(){
            return "Marque : ".$this->_marque." Modèle : ".$this->_modele;
        }
    }

    class VoitureElec extends Voiture {
        private int $_autonomie;

        public function __construct(string $marque, string $modele, int $autonomie){
            parent::__construct($marque, $modele);
            $this->_autonomie = $autonomie;
        }

        public function getAutonomie(){
            return $this->_autonomie;
        }

        public function setAutonomie($autonomie){
            $this->_autonomie = $autonomie;
        }

        public function getInfos(){
            return parent::getInfos()."<br>Autonomie : $this->_autonomie KM";
        }
    }

    $v1 = new Voiture("Peugeot","408");

    $ve1 = new VoitureElec("BMW","I3",100);

    echo $v1->getInfos()."<br/>";
    echo $ve1->getInfos()."<br/>";

?>