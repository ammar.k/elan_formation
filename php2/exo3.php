<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>3- Exercice 3</h1>

        <p>Afficher un lien hypertexte permettant d’accéder au site d’Elan Formation. Le lien
             devra s’ouvrir dans un nouvel onglet (_blank).</p>
</body>
</html>

<?php

function lienElan(){
    return "<a href='https://www.elan-formation.fr/' target='_blank'>Elan Formation</a>";
    // target='_blank' pour ouvrir dans un nouvel onglet
}

echo lienElan();

?>