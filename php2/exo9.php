<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>9- Exercise 9</h1>
    <p>
Créer une fonction personnalisée permettant d’afficher des boutons radio
avec un tableau de valeurs en paramètre ("Monsieur","Madame","Mademoiselle").
Exemple :
afficherRadio($nomsRadio);</p>
</body>
</html>

<?php
    function afficheRadio($nomsRadio){
        $results ="<label for='$nomsRadio'>choose please :</label><br>";
        foreach($nomsRadio as $value){
            $results.= "<input type='radio' name='$value' value='$value'>$value<br>";
        }
        $results.= "<input type='submit' value='Envoyer'>";
        return $results;
    }

    $nomsRadio = array("Monsieur","Madame","Mademoiselle");
    echo afficheRadio($nomsRadio);

?>