<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>13- Exercice 13</h1>
    <p>Créer une classe Voiture possédant les propriétés suivantes : marque, modèle,
        nbPortes et vitesseActuelle ainsi que les méthodes demarrer( ), accelerer( ) et
        stopper( ) en plus des accesseurs (get) et mutateurs (set) traditionnels.
        La vitesse initiale de chaque véhicule instancié est de 0.
        Une méthode personnalisée pourra afficher toutes les informations d’un véhicule.
        v1 ➔ "Peugeot","408",5 v2 ➔ "Citroën","C4",3
        Coder l’ensemble des méthodes, accesseurs et mutateurs
        de la classe tout en réalisant des jeux de tests pour vérifier la cohérence
        de la classe Voiture. Vous devez afficher les tests et les éléments suivants :</p>
</body>
</html>

<?php
    class Voiture {
        private string $_marque;
        private string $_modele;
        private int $_nbPortes;
        private int $_vitesseActuelle;

        public function __construct(string $marque, string $modele, int $nbPortes){
            $this->_marque = $marque;
            $this->_modele = $modele;
            $this->_nbPortes = $nbPortes;
            $this->_vitesseActuelle = 0;
        }

        public function getMarque(){
            return $this->_marque;
        }

        public function setMarque($marque){
            $this->_marque = $marque;
        }

        public function getModele(){
            return $this->_modele;
        }

        public function setModele($modele){
            $this->_modele = $modele;
        }

        public function getNbPortes(){
            return $this->_nbPortes;
        }

        public function setNbPortes($nbPortes){
            $this->_nbPortes = $nbPortes;
        }

        public function demarrer(){
            echo "La voiture démarre<br>";
        }

        public function accelerer(){
            echo "La voiture accélère<br>";
            $this->_vitesseActuelle += 10;
            echo "La vitesse actuelle est de $this->_vitesseActuelle<br>";
        }

        public function stopper(){
            echo "La voiture s'arrête<br>";
            $this->_vitesseActuelle = 0;
            echo "La vitesse actuelle est de $this->_vitesseActuelle<br>";
        }

        public function afficherInfos(){
            echo "La marque est $this->_marque, $this->_modele, $this->_nbPortes<br>";
            
        }


    }

    $v1 = new Voiture("Peugeot", "408", 5);
    $v2 = new Voiture("Citroën", "C4", 3);
    $v1->demarrer();
    $v1->accelerer();
    $v1->accelerer();
    $v1->accelerer();
    $v1->afficherInfos();
    $v1->stopper();
    $v2->demarrer();
    $v2->accelerer();
    $v2->accelerer();
    $v2->afficherInfos();
    $v2->stopper();

?>