<?php

class Auteur{

    private string $_nom;
    private string $_prenom;

    public function __construct(string $nom, string $prenom){
        $this->_nom = $nom;
        $this->_prenom = $prenom;
    }
    
 
    public function getNom()
    {
        return $this->_nom;
    }


    public function setNom(string $_nom)
    {
        $this->_nom = $_nom;

        return $this;
    }

    public function getPrenom()
    {
        return $this->_prenom;
    }

 
    public function setPrenom(string $_prenom)
    {
        $this->_prenom = $_prenom;

        return $this;
    }

    public function __toString()
    {
        return $this->_nom . " " . $this->_prenom. "<br>";
    }

}



?>