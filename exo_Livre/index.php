<h1>1- Exercice 1</h1>

<h2>Livres et Autors</h2>

<?php

//require "Author.php";
//require "Livre.php";

spl_autoload_register(function ($class_name) {
    require $class_name . '.php';
});


$auteur1 = new Auteur("King", "Stephen");
echo $auteur1;

$book1 = new Livre("Shining", "1977", 447, 8.20, $auteur1);
$book2 = new Livre("Doctor Sleep", "2013", 544, 9.90, $auteur1);
$book3 = new Livre("Ça", "1986", 1138, 9.90, $auteur1);
$book4 = new Livre("Simetierre", "1983", 568, 8.20, $auteur1);
$book5 = new Livre("Misery", "1987", 544, 7.90, $auteur1);

echo $book1;
echo $book2;
echo $book3;
echo $book4;
echo $book5;

?>